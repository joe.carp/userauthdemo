#pragma once

#define TWO_FACTOR_ENABLED false         // Not yet implemented, leave disabled.
#define FORCE_TWO_FACTOR_ENABLED false   // Force new users to start with two-factor enabled.
#define STORE_FILE L"/UserAuthDemo/UserRecords.json" // used multiple locations, avoid magic.