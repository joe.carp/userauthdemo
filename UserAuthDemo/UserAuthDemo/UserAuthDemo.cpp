// UserAuthDemo.cpp
//
/*

DISCLAIMER: If I were actually intending to trust an authentication system with customer data I wouldn't write it myself unless I absolutely had to.
Google and others have some rather nice off-the-shelf solutions which will not only be vulnerable to me making security-newbie mistakes, but move the
liability concerns to a much larger entity.  If I create a system from scratch and there's a breach, I'm screwed.  If I use Google's system and
There's a breach I'm covered.  (Or at least much more covered.)


I/O: here it is simple, on a larger system there could be multiple subsystems needing to authenticate against the same list of users/passwords

Backing store: Ideally a database, at least for larger systems.  Should be abstracted away

Two factor is nice, but sms costs money.  Allow for later update to include.  (need field for user phone# in user record, also a flag for whether it's enabled for that user.  May be left empty at present)
    Two factor may be something other than SMS, such as a code generation card like SurePassID.  Try to allow for alternate extensions.

Consider Hash strength.
    Reference site: https://crackstation.net/hashing-security.htm
    Windows API has a "bcrypt" but it's not actually bcrypt "BestCrypt" vs "Blowfish Crypt"
    libbcrypt on github has an implementation that's usable.

Keyed hash?
    Can't afford special hardware, but consider extensibility.

New user creation
    Name collisions
    Password strength validation
    create salt
        CryptGenRandom
    Permissions list?  (Not in version 1, but allow for expansion)

User authentication
    Intake username & password
    retrieve salt
    hash
    compare

*/


#include "pch.h"
#include <windows.h>
#include <Wincrypt.h>
#include <stdio.h>
#include <bcrypt.h>
#include <iostream>
#include <regex>
#include <string>
#include "UserRecordHandler.h"
#include "bcrypt/BCrypt.hpp"

using namespace std;

UserRecordHandler records;

string makeSalt() {
    // Modern Intel chips have a true (if slow) RNG available, but I shouldn't assume its availability here.
    // Using windows CryptGenRandom instead.
    HCRYPTPROV hCryptProv;

    if (!CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, 0))
    {
        // First attempt unsuccessful.  Possible empty default?
        if (!CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET)) {
            cout << "Unable to acquire crypt provider" << endl;
            exit(EXIT_FAILURE);
        }
    }
    // Generate random data
    BYTE salt[64];
    CryptGenRandom(hCryptProv, 64, salt);
    // turn into string we can use and store.
    DWORD bufferNeeded;
    CryptBinaryToStringA(salt, 64, CRYPT_STRING_BASE64HEADER | CRYPT_STRING_NOCRLF, NULL, &bufferNeeded);
    char* buffer = new char[bufferNeeded];
    CryptBinaryToStringA(salt, 64, CRYPT_STRING_BASE64HEADER | CRYPT_STRING_NOCRLF, buffer, &bufferNeeded);
    string saltString;
}

// getpassword function copied from cplusplus.com.  This is not secure against software keyloggers.  It does not disable hooks.  All it does is prevent someone looking over your shoulder.
// I need to learn quite a bit more about how all of those things work before I can attempt a secure password input prompt.  I am, however, aware of (some of) the threats.
string getpassword(const string& prompt = "Enter password> ")
{
    string result;

    // Set the console mode to no-echo, not-line-buffered input
    DWORD mode, count;
    HANDLE ih = GetStdHandle(STD_INPUT_HANDLE);
    HANDLE oh = GetStdHandle(STD_OUTPUT_HANDLE);
    if (!GetConsoleMode(ih, &mode))
        throw runtime_error(
            "getpassword: You must be connected to a console to use this program.\n"
        );
    SetConsoleMode(ih, mode & ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT));

    // Get the password string
    WriteConsoleA(oh, prompt.c_str(), prompt.length(), &count, NULL);
    char c;
    while (ReadConsoleA(ih, &c, 1, &count, NULL) && (c != '\r') && (c != '\n'))
    {
        if (c == '\b')
        {
            if (result.length())
            {
                WriteConsoleA(oh, "\b \b", 3, &count, NULL);
                result.erase(result.end() - 1);
            }
        }
        else
        {
            WriteConsoleA(oh, "*", 1, &count, NULL);
            result.push_back(c);
        }
    }

    // Restore the console mode
    SetConsoleMode(ih, mode);

    return result;
}

void createNewUser() {
    // Create new user.

    string userNameTyped = "";
    string userPassTyped;
    string userFullNameTyped;
    string PhoneTyped = "";


    while (userNameTyped.length() == 0) {
        cout << "Please enter username" << endl;
        getline(cin, userNameTyped);
        if (records.isUser(userNameTyped)) {
            userNameTyped = ""; // clear the input
            cout << "That username is already in use.  Please choose a different username." << endl;
        }
        
    }

    userPassTyped = getpassword("Please enter password.\n");

    cout << "Please enter your full name." << endl;
    getline(cin, userFullNameTyped);

    if (TWO_FACTOR_ENABLED) {
        if (FORCE_TWO_FACTOR_ENABLED) {
            cout << "Please enter your phone number for SMS authentication." << endl;
            getline(cin, PhoneTyped);
        }
        else {
            cout << "Please enter your phone number for SMS authentication, or leave blank to forgo additional security." << endl;
            getline(cin, PhoneTyped);
        }
    }



    records.addNewUser(userNameTyped, userFullNameTyped, )
    

}

int main()
{
    bool bAuthenticated = false;

    string userNameTyped;
    string userPassTyped;

    while (!bAuthenticated) {

        cout << "Please enter username or type \"new\" to create a new user." << endl;
        getline(cin, userNameTyped);

        if (userNameTyped.compare("new") == 0) {
            createNewUser();
        }

        userPassTyped = getpassword("Please enter password.\n");

        // Check if they're actually a user already.
        if (!records.isUser(userNameTyped)) {
            // Not an active user, prompt for account creation
            cout << "User not found.  Please check your spelling or type \"new\" to create a new user." << endl;

        }

        UserRecord currentUser = records.getUserRecord(userNameTyped);

    } 
}

