

#include "pch.h"
#include "shlobj.h"
#include "shlwapi.h"
#include "UserRecordHandler.h"
#include "json.hpp" // Credit to Niels Lohmann and his team.  The library is MIT license.  Thanks guys.

// for convenience
using json = nlohmann::json;

using namespace std;

/*Begin UserRecord*/

UserRecord::UserRecord()
{
}

UserRecord::UserRecord(string userName, string userFullName, string salt, string saltedHash, string userPhone, bool enableTwoFactor)
{
    UserName = userName;
    UserFullName = userFullName;
    Salt = salt;
    SaltedHash = saltedHash;
    if (userPhone.length() > 0) {
        Phone = userPhone;
        if (enableTwoFactor) TwoFactorEnabled = true; // Need phone# in order to enable two-factor.
    } else TwoFactorEnabled = false;
}

const string UserRecord::getUserName()
{
    return UserName;
}

const string UserRecord::getUserFullName()
{
    return UserFullName;
}

const string UserRecord::getSalt()
{
    return Salt;
}

const string UserRecord::getPhone()
{
    return Phone;
}

bool UserRecord::getTwoFactorEnabled()
{
    return false;
}

bool UserRecord::testSaltedHash(string inputSaltedHash)
{
    if (inputSaltedHash.compare(SaltedHash) == 0) return true;
    return false;
}


void to_json(json& j, const UserRecord& p) {
    j = json{ {"UserType", p.UserType},{"UserName", p.UserName},{"UserFullName", p.UserFullName},{"SaltedHash", p.SaltedHash},{"Salt", p.Salt},{"Phone", p.Phone},{"TwoFactorEnabled", p.TwoFactorEnabled} };
}

void from_json(const json& j, UserRecord& p) {
    j.at("UserType").get_to(p.UserType);
    j.at("UserName").get_to(p.UserName);
    j.at("UserFullName").get_to(p.UserFullName);
    j.at("SaltedHash").get_to(p.SaltedHash);
    j.at("Salt").get_to(p.Salt);
    j.at("Phone").get_to(p.Phone);
    j.at("TwoFactorEnabled").get_to(p.TwoFactorEnabled);
}



/*End UserRecord*/



/*Begin UserRecordHandler*/

UserRecordHandler::UserRecordHandler()
{
    //Need to initialize the backing store.  For this implementation this means we need to read from the file where we stored our user data.
    wchar_t localAppData[MAX_PATH];
    if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, localAppData))) {
        // Ok, we have a folder where we should have read/write access.
        std::wstringstream filePathStream;

        filePathStream << localAppData << STORE_FILE; // Make a file path for a file where we stored the data (unless this is the first run.)

        // Test existence
        if (!PathFileExists(filePathStream.str().c_str())) {
            // No existing records to read in.  Just initialize an empty table and return.
            RecordStore.clear();
            return;
        }
        else {
            // We have entries to load.
            InitRecordStoreFromFile(filePathStream.str());
        }

    }
    else {
        cout << "Unable to access %AppDataLocal%\n";
        exit(EXIT_FAILURE);
    }
}

UserRecordHandler::~UserRecordHandler()
{
    // On a deployed system we should use a database not a flat file.  That way we would continuously write new user records to long term storage
    // And we'd have transactions too, which would be beneficial.
    // Here we're just writing the file out for our "save&exit"
    // Need to initialize the backing store.  For this implementation this means we need to read from the file where we stored our user data.

    wchar_t localAppData[MAX_PATH];
    if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, localAppData))) {
        std::wstringstream filePathStream;
        filePathStream << localAppData << STORE_FILE; // Make a file path for a file where we stored the data (unless this is the first run.)

        SerializeRecordStoreToFile(filePathStream.str());
    }

}

UserRecord UserRecordHandler::getUserRecord(string userName)
{
    // If we had a database backing this we'd be making a SELECT * FROM users WHERE UserName = userName;
    // And then calling createUserRecord to make a record to hand back.
    // As is, we have a table stored locally.

    auto recordIter = RecordStore.find(userName);
    return recordIter->second;
}

bool UserRecordHandler::isUser(string userName)
{
    return (RecordStore.find(userName) != RecordStore.end());
}

bool UserRecordHandler::addNewUser(string userName, string userFullName, string salt, string saltedHash, string userPhone, bool enableTwoFactor)
{
    //Create the record
    UserRecord record = createUserRecord(userName, userFullName, salt, saltedHash, userPhone, enableTwoFactor);
    // Add it to the store.
    try {
        RecordStore.emplace(userName, record);
    }
    catch (exception e) {
        // If an exception was thrown by emplace, we failed to add the record to the store.
        return false;
    }
    return true;
}

UserRecord UserRecordHandler::createUserRecord(string userName, string userFullName, string salt, string saltedHash, string userPhone, bool enableTwoFactor, eUserType userType)
{
    // For now there's just the one type of user record to create, but eventually there could be more.
    if (userType == eUserType::defaultUser) {
        return UserRecord(userName, userFullName, salt, saltedHash, userPhone, enableTwoFactor);
    }
    else {
        cerr << "Invalid user type encountered\n";
        // Attempt to gracefully handle the use case.
        return UserRecord(userName, userFullName, salt, saltedHash, userPhone, enableTwoFactor);
    }
}

void UserRecordHandler::InitRecordStoreFromFile(wstring filepath)
{
    // Time to de-serialize the file.
    fstream recordFileStream(filepath, ios::in);
    json jsonData;
    recordFileStream >> jsonData;
    for (json::iterator it = jsonData.begin(); it != jsonData.end(); ++it) {
        UserRecord record = (*it).get<UserRecord>();
        RecordStore.emplace(record.getUserName(), record);
    }
    recordFileStream.close(); // Close the file handle.
}


void UserRecordHandler::SerializeRecordStoreToFile(wstring filepath) {
    // Take the RecordStore and feed it out to a file through json.
    fstream recordFileStream(filepath, ios::out);
    
    for (auto it = RecordStore.begin(); it != RecordStore.end(); ++it) {
        json jRecord = json(*it);
        recordFileStream << jRecord;
    }
    recordFileStream.close(); // Close the file handle.
}


