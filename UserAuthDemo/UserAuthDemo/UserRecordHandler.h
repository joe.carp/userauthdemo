#pragma once


#include "pch.h"
#include "defines.h"
#include <windows.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>
#include <string>
#include <unordered_map>
#include "shlobj.h"
#include "shlwapi.h"
#include "json.hpp" // Credit to Niels Lohmann and his team.  The library is MIT license.  Thanks guys.

// for convenience
using json = nlohmann::json;

using namespace std;

enum eUserType {
    defaultUser
};

class UserRecord {
protected:
    eUserType   UserType = defaultUser;                     // If we eventually have multiply 'types' of users, we'll need to be able to store and retrieve which type of user they are from whatever backing store we may have.
    string      UserName;                                   // The name they want associated with their account.  Must be unique, can be used as primary key.
    string      UserFullName;                               // Don't assume first/middle/last, may have more or fewer names/titles, MAY NOT BE UNIQUE! DO NOT USE AS KEY!
    string      SaltedHash;
    string      Salt;                                       // Generate using secure PRNG
    string      Phone;                                      // For two-factor use.
    bool        TwoFactorEnabled;


    bool setUsername() { return false; };               // No changing your username for now, make a new acct.

    friend bool passwordReset(string newPassword);  // When a user resets their password, we should be able to actually do that.
    bool changePassword(string newSaltedHash, string newSalt); // Need to change the salt when changing passwords.  Forces rainbow table creation to start from scratch if compromised.

public:
    UserRecord(); // Need default constructor for nlhomann json
    UserRecord(string userName, string userFullName, string salt, string saltedHash, string userPhone = "", bool enableTwoFactor = TWO_FACTOR_ENABLED);
    // These need getters
    const string getUserName();
    const string getUserFullName();
    const string getSalt();
    const string getPhone();
    bool   getTwoFactorEnabled();

    bool testSaltedHash(string inputSaltedHash); // Don't need to retrieve, just need to check.

    // Functions for serializatin/deserialization with nlohmann JSON library
    friend void to_json(json& j, const UserRecord& p);
    friend void from_json(const json& j, UserRecord& p);
};

class UserRecordHandler {
    // Communicates with the backing store.
    // Ideally this is a database, but for now it's something simpler.
    // All communication with the backing store should go through here, so this is the only thing that needs to be re-written if things change.
private:
    unordered_map<string, UserRecord> RecordStore; // The key will be the username

    // Before two-factor is implemented, do not force phone number to be filled in.
    // Once two-factor is implemented, default to enabled by changing define.
    UserRecord createUserRecord(string userName,
                                string userFullName,
                                string salt,
                                string saltedHash,
                                string userPhone = "",
                                bool enableTwoFactor = TWO_FACTOR_ENABLED,
                                eUserType userType = eUserType::defaultUser);
    void InitRecordStoreFromFile(wstring filepath); // filepaths in windows are LPCWSTR, so we need to keep the widechars around to interact with the API more cleanly.
    void SerializeRecordStoreToFile(wstring filepath);


public:
    UserRecordHandler();
    ~UserRecordHandler();

    UserRecord getUserRecord(string userName);

    bool isUser(string userName);

    bool addNewUser(string userName, string userFullName, string salt, string saltedHash, string userPhone = "", bool enableTwoFactor = TWO_FACTOR_ENABLED);


};